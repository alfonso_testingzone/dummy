package mx.com.allianz.reportes.service.io;

import mx.com.allianz.commons.dto.daf.PolizaDTO;

public interface ReporteCuestionariosPdfService {

	public String crearPdf(PolizaDTO poliza);
	public boolean guardarPDF(String PdfBase64, String idPoliza, String idCuestionario);
	public String obtenerPolizaPDF(String idPoliza, String idCuestionario);
}
