package mx.com.allianz.reportes.service.controller;

	import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.GitProperties;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import mx.com.allianz.commons.dto.daf.AportacionDTO;
import mx.com.allianz.commons.dto.daf.BeneficiarioDTO;
import mx.com.allianz.commons.dto.daf.CatalogoDTO;
import mx.com.allianz.commons.dto.daf.CatalogosDTO;
import mx.com.allianz.commons.dto.daf.ClienteDTO;
import mx.com.allianz.commons.dto.daf.ContactoDTO;
import mx.com.allianz.commons.dto.daf.CuestionarioDTO;
import mx.com.allianz.commons.dto.daf.DatosMedicosDTO;
import mx.com.allianz.commons.dto.daf.PolizaDTO;
import mx.com.allianz.commons.dto.daf.ProductoDTO;
import mx.com.allianz.commons.dto.daf.domicilio.DireccionDTO;
import mx.com.allianz.reportes.service.factory.ReportePdf;
import mx.com.allianz.reportes.service.factory.impl.ReporteCuestionariosPdf;
import mx.com.allianz.reportes.service.factory.impl.ReportePolizaPDF;
import mx.com.allianz.reportes.service.io.ReporteCuestionariosPdfService;
import mx.com.allianz.reportes.service.io.ReportePolizaPdfService;


@RestController
public class ReportesController {
	
	@Autowired
	ReportePolizaPdfService reporteador;
	
	@Autowired
	ReporteCuestionariosPdfService reporteadorCuesitonarios;
	
	@RequestMapping(value = "/genera-reporte", method = RequestMethod.POST)
	@ResponseBody
    public Response generarReporte(
    		@RequestBody PolizaDTO poliza
    		) {
		System.out.println("Objeto obtenido:" + poliza);
		generaPolizaPDF(  poliza );
		generaCuestionariosPDF(poliza);
		enviarCorreo( poliza );
          //  System.out.println("Listo!!!!!!");

//        try {
//            file = new File(FILE);
//	        int length = (int) file.length();
//	        reader = new BufferedInputStream(new FileInputStream(file));
//	        bytes = new byte[length];
//	        reader.read(bytes, 0, length);
//	        reader.close();  
//            
//			pdf.put("pdf", Base64.getEncoder().encodeToString(bytes));
//			System.out.println("Reporte encoded a Base64 correctamente");
//		} catch (JSONException e) {
//			e.printStackTrace();
//		} catch (Exception e){
//			e.printStackTrace();
//		}

        return Response.status(200).entity("{\"pinchejson\":\"nadieTePela\"}").build();

    }	
	
	
	private void generaPolizaPDF(PolizaDTO poliza){
		reporteador.crearPdf(poliza);
		File sourcefile = new File(ReportePdf.PDF_ROOTPATH + ReportePolizaPDF.FILE_POLIZA);
		FileInputStream fin;
		try {
			fin = new FileInputStream(sourcefile);
			byte[] fileContent = new byte[(int)sourcefile.length()];
			fin.read(fileContent);
			String pdfBase64 = Base64.getEncoder().encodeToString(fileContent);
			reporteador.guardarPDF(pdfBase64, poliza.getIdSolicitudPoliza());
			//sourcefile.delete(); TODO DEscomentar
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	private void generaCuestionariosPDF(PolizaDTO poliza){
		String pdfBase64;
		File sourcefile ;
		FileInputStream fin;
		byte[] fileContent;
		
		try {
			reporteadorCuesitonarios.crearPdf(poliza);
			for (CuestionarioDTO cuestionario : poliza.getCuestionario()){
				
				sourcefile = new File(ReportePdf.PDF_ROOTPATH + cuestionario.getTipoCuestionario() + ReporteCuestionariosPdf.FILE_CUESTIONARIOS);
				fin = new FileInputStream(sourcefile);
				fileContent = new byte[(int)sourcefile.length()];
				fin.read(fileContent);
				pdfBase64 = Base64.getEncoder().encodeToString(fileContent);
				reporteadorCuesitonarios.guardarPDF(pdfBase64, poliza.getIdSolicitudPoliza(),cuestionario.getTipoCuestionario());
				//sourcefile.delete(); TODO descomentar 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main (String [] args){
		
		
		ReportesController builderService = new ReportesController();
		//PolizaDTO poliza = builderService.creaPolizaDummy();
		//System.out.println("poliza:" + poliza);
//		builderService.generaPolizaPDF( poliza );
//		builderService.generaCuestionariosPDF(poliza);
		//System.out.println(builderService.leerArchivo());
		
		
	}
	
	
	private PolizaDTO creaPolizaDummy(){
		PolizaDTO poliza = new PolizaDTO();
		ClienteDTO cliente = new ClienteDTO();
		DireccionDTO domicilio = new DireccionDTO();
		ContactoDTO contacto = new ContactoDTO("prueba@gmail.com", "5566778899",true,true);
		List<CuestionarioDTO> cuestionarios = new ArrayList<>();
		AportacionDTO aportacion = new AportacionDTO();
		
		poliza.setCodProd("Optimaxx plus –Seguro de Vida Inversión");
		
		domicilio.setCalle("Mar marmara");
		domicilio.setNumeroExterior("352");
		domicilio.setColonia(new CatalogoDTO(0, "Popotla"));
		domicilio.setCodigoPostal(11100);
		domicilio.setMunicipio(new CatalogoDTO(0, "Miguel Hidalgo"));
		domicilio.setCiudad(new CatalogoDTO(0, "Distrito Federal"));
		domicilio.setEstado(new CatalogoDTO(0,"Ciudad de México"));
		domicilio.setPais(new CatalogoDTO(0, "Mexico"));
		
		cliente.setApellidoMaterno("Castillo");
		cliente.setApellidoPaterno("Lecona");
		cliente.setNombre("Cesar");
		cliente.setGenero(new CatalogosDTO("H", "Masculino"));
		cliente.setNacionalidad(new CatalogosDTO("o", "Mexicano"));
		poliza.setContacto(contacto);

		
		aportacion.setProductos(new ProductoDTO(null,"Optimaxx plus –Seguro de Vida Inversión",null));
		poliza.setAportacion(aportacion);
		
		poliza.setDatosMedicos(new DatosMedicosDTO( 70, 1.77));
		
		poliza.setBeneficiario(new ArrayList<>());
		poliza.getBeneficiario().add(new BeneficiarioDTO( 90d, "lelelelel", new CatalogoDTO(0, "Papá"),"Pepe Pecas"));
		poliza.getBeneficiario().add(new BeneficiarioDTO( 10d, "kekekekkeke", new CatalogoDTO(0, "Mamá"),"Pepa Peces"));
		
		try {
			cliente.setFechaNacimiento(new SimpleDateFormat("yyyy-MM-dd").parse("1989-12-12"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		cuestionarios = leerArchivo();
		System.out.println("------------");
		
		System.out.println(cuestionarios);
		
		
		poliza.setCuestionario(cuestionarios);
		poliza.setDomicilio(domicilio);
		poliza.setCliente(cliente);
		return poliza;
	}
	
	private List<CuestionarioDTO> leerArchivo(){
		
		String cadena = null;
        FileReader f;
        BufferedReader b = null;
        StringBuilder json = new StringBuilder();
        Gson gson = new Gson();
        
        CuestionarioDTO[] cuest = null;
		try {
			f = new FileReader("src/main/resources/cuestionarios.js");
			b = new BufferedReader(f);
			while((cadena = b.readLine())!=null) {
				json.append(cadena);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if ( b != null){
				try {
					b.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println("La cadena regresa" );
		
		cuest = gson.fromJson(json.toString(), CuestionarioDTO[].class);
		
		System.out.println("Objeto parseado :" + Arrays.asList(cuest));
		
		
		return Arrays.asList(cuest);
	}
	
	@RequestMapping(value = "/guardar-pdf")
	//@ResponseBody
	public Response guardaPolizaPDF(String idPoliza){
		String pdfBase64 = "";
		FileInputStream fin;
		byte fileContent[];
		File sourceDir = new File(ReportePdf.PDF_ROOTPATH );
		File sourcefile = new File(ReportePdf.PDF_ROOTPATH + ReportePolizaPDF.FILE_POLIZA);
		
		try {
			if (sourceDir.isDirectory()){
				for (File file :sourceDir.listFiles()){
					fin = new FileInputStream(sourcefile);
					fileContent = new byte[(int)sourcefile.length()];
					fin.read(fileContent);
					pdfBase64 = Base64.getEncoder().encodeToString(fileContent);
					System.out.println("Se va a guardar el pdf" + file.getName() + " con un string de longitud:" + pdfBase64.length()+" y id:"+idPoliza);
					if (file.getName().startsWith(ReportePolizaPDF.FILE_POLIZA)){
						reporteador.guardarPDF(pdfBase64, idPoliza);
					} else {
						reporteadorCuesitonarios.guardarPDF(pdfBase64, idPoliza, file.getName());
					}
				}
				
				for (File file : sourceDir.listFiles()){
					file.delete();
				}
				
			}
			System.out.println("Ejecucion correcta");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(200).entity("{\"msg\":\"Guardado correcto\"}").build();
	}
	
	@RequestMapping(value = "/obtener-pdf", method = RequestMethod.GET)
	@ResponseBody
	public Response obtenerPolizaPDF(String idPoliza){
		
		String reporteBase64 = reporteador.obtenerPolizaPDF(idPoliza);
		
		System.out.println("Se obtuvo el pdf con un string de longitud:" + reporteBase64.length());
		
		return Response.status(200).entity("{\"msg\":\"Guardado correcto\"}").build();
	}
	
	@RequestMapping(value = "/enviar-correo", method = RequestMethod.POST)
	@ResponseBody
	public Response enviaCorreo(PolizaDTO poliza){
		
		System.out.println("Se ejecutara el envio de correo --------");
		System.out.println("--------Objeto recibido para envio de correo:" +poliza);
		
		enviarCorreo( poliza );
		
		return Response.status(200).entity("{\"msg\":\"Envio correcto\"}").build();
	}
	
	public void enviarCorreo(PolizaDTO poliza){
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap(); 
		mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html"); 
		mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml"); 
		mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain"); 
		mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed"); 
		mc.addMailcap("message/rfc822;; x-java-content- handler=com.sun.mail.handlers.message_rfc822");
		
		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("testinfzoneallianz@gmail.com",
							"T3$T!NGZ0N3");
				}
			});

		try {
			String from = "contacto@allianz.com.mx";
			System.out.println("Correo recibido" + poliza.getContacto().getCorreo()
					);
			String to = poliza.getContacto().getCorreo();
			String subject = "Emision poliza ";
			
			//String body = "body";
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			addCopies(Optional.of(new String[]{to}), Message.RecipientType.TO, message);
			addCopies(Optional.of(new String[]{"b4ld0r@gmail.com"}), Message.RecipientType.BCC, message);

			message.setSubject(subject);

			// Create the message part
	         BodyPart messageBodyPart = new MimeBodyPart();

	         // Now set the actual message
	         messageBodyPart.setText("Emision poliza de " + poliza.getCliente().getNombre()+" "
	        		 + poliza.getCliente().getApellidoPaterno()+" "+ poliza.getCliente().getApellidoMaterno());

	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();

	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			
			String filename = ReportePdf.PDF_ROOTPATH + ReportePolizaPDF.FILE_POLIZA;
			DataSource source = new FileDataSource(filename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(ReportePolizaPDF.FILE_POLIZA);
			multipart.addBodyPart(messageBodyPart);
			
			//message.setContent( multipart );
			
			if (poliza.getCuestionario() != null && ! poliza.getCuestionario().isEmpty() )
				for (CuestionarioDTO cuestionario:poliza.getCuestionario()){
					//multipart = new MimeMultipart();

			         // Set text message part
			        // multipart.addBodyPart(messageBodyPart);

					messageBodyPart = new MimeBodyPart();
					
					filename = ReportePdf.PDF_ROOTPATH + cuestionario.getTipoCuestionario() + ReporteCuestionariosPdf.FILE_CUESTIONARIOS;
					if (new File(filename).exists()){
						source = new FileDataSource(filename);
						messageBodyPart.setDataHandler(new DataHandler(source));
						messageBodyPart.setFileName(cuestionario.getTipoCuestionario() + ReporteCuestionariosPdf.FILE_CUESTIONARIOS);
						multipart.addBodyPart(messageBodyPart);
						
						
					}else {
						System.out.println("El cuestionario " + cuestionario.getTipoCuestionario() + " viene en el objeto poliza pero no se generó el pdf :(");
					}
				}
			message.setContent( multipart );
			
//			message.setContent(body, "text/html; charset=ISO-8859-1");
		    message.saveChanges();

			Transport.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private Optional<InternetAddress[]> getInternetAddresses(String mails) {
		Optional<InternetAddress[]> internetAddress = Optional.empty();
		if (mails != null){
			try {
				internetAddress = Optional.ofNullable(InternetAddress.parse(mails));
			} catch (AddressException e) {
				e.printStackTrace();
			}
		};
		return internetAddress;
	}

	private void addCopies(Optional<String[]> mailArray, Message.RecipientType type, Message message) {
		mailArray.map(Arrays::asList).map(list -> String.join(",", list)).
		flatMap(mails -> getInternetAddresses(mails)).ifPresent(internetAddresses -> { try {
			message.addRecipients(type,internetAddresses);
		} catch (MessagingException e) { e.printStackTrace(); }
		});
	}
	
    
}
