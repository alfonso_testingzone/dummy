package mx.com.allianz.reportes.service.factory.impl;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lowagie.text.Chapter;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Section;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import mx.com.allianz.commons.dto.daf.CuestionarioDTO;
import mx.com.allianz.commons.dto.daf.PolizaDTO;
import mx.com.allianz.commons.dto.daf.RespuestaDTO;
import mx.com.allianz.reportes.service.dao.PdfPolizaDao;
import mx.com.allianz.reportes.service.factory.ReportePdf;
import mx.com.allianz.reportes.service.io.ReporteCuestionariosPdfService;
import mx.com.allianz.reportes.service.model.PdfPoliza;
import mx.com.allianz.reportes.service.model.PdfPolizaPk;

@Service
public class ReporteCuestionariosPdf extends ReportePdf implements ReporteCuestionariosPdfService{
	
	@Autowired
	private PdfPolizaDao pdfPolizaDao;
	
	public static String FILE_CUESTIONARIOS = "_pdf.pdf";
	private static final String CLAVE_PREGUNTA_BOOLEANA = "1";
	
  	@Override
	public String crearPdf(PolizaDTO poliza) {
		PdfPCell cell ;
    	PdfPTable table;
    	List<CuestionarioDTO> cuestionarios = poliza.getCuestionario() != null ? poliza.getCuestionario() : new ArrayList<>();
    	
    	
    	HeaderFooter event = new HeaderFooter("\n" + (poliza.getAportacion() != null && poliza.getAportacion().getProductos() != null ? 
    			poliza.getAportacion().getProductos().getDescProd() : VALOR_NULO));
		
    	if ( cuestionarios  != null && !cuestionarios.isEmpty() )
    		for ( CuestionarioDTO cuestionario : cuestionarios){
		
				try {
		            Document document = new Document();
		            PdfWriter writer = PdfWriter.getInstance(document, 
		            		new FileOutputStream( PDF_ROOTPATH + cuestionario.getTipoCuestionario() + FILE_CUESTIONARIOS ));
		            document.open();
		            writer.setPageEvent(event);
		            addMetaData(document, poliza);
		
		            /////
		            Chapter catPart = new Chapter( 1 );
		            catPart.setNumberDepth(0);
		
		            Section subCatPart = catPart.addSection("");
		            subCatPart.setNumberDepth(0);
		            
		            subCatPart.add(addHeader( "\n" + (poliza.getAportacion() != null && poliza.getAportacion().getProductos() != null ? 
		            		poliza.getAportacion().getProductos().getDescProd() : VALOR_NULO)));
		            
		    	    		table = new PdfPTable(new float[] { 5, 1, 1 });
		    	            table.setWidthPercentage(100);
		    	            table.addCell( crearCeldaTitulo(cuestionario.getTituloCuestionario(),3));
		    	            
		    	            if ( cuestionario.getRespuesta() != null && !cuestionario.getRespuesta().isEmpty() )
		    		            for ( RespuestaDTO respuesta : cuestionario.getRespuesta() ){
		    		            	table.addCell(crearCeldaAzulita(crearParagraph(respuesta.getDescripcionPregunta(), null, smallNormal)));
		    		            	if ( CLAVE_PREGUNTA_BOOLEANA.equals(respuesta.getTipoPregunta()) ){
		    		            		table.addCell(crearCeldaAzulita(crearOpcionCheck("Sí ", Boolean.valueOf(respuesta.getValorRespuesta()), smallNormal)));
		    		            		table.addCell(crearCeldaAzulita(crearOpcionCheck("No ", !Boolean.valueOf(respuesta.getValorRespuesta()), smallNormal)));
		    		            	} else {
		    		            		cell = crearCeldaAzulita(crearParagraph(respuesta.getDescripcionRespuesta(), null, smallNormal));
		    		            		cell.setColspan(2);
		    		            		table.addCell(cell);
		    		            	}
		    		            }
		    	            subCatPart.add(table);
		    	            subCatPart.add(new Paragraph(" "));
		            
		            // now add all this to the document
		            document.add(catPart);
		            
		            ////
		            
		            document.close();
		        } catch (Exception e) {
		        	System.out.println("Tronó:"+e.getMessage());
		        	e.printStackTrace();
		        }
				System.out.println("Reporte "+cuestionario.getTipoCuestionario()+" creado exitosamente");
    		}
		return null;
	}

	@Override
	public boolean guardarPDF(String PdfBase64, String idPoliza, String idCuestionario) {
		PdfPoliza poliza = new PdfPoliza(idPoliza,idCuestionario,PdfBase64);
		poliza = pdfPolizaDao.save(poliza);
		return poliza != null;
	}

	@Override
	public String obtenerPolizaPDF(String idPoliza, String idCuestionario) {
		PdfPoliza poliza = pdfPolizaDao.findOne(new PdfPolizaPk(idPoliza, idCuestionario));
		return poliza.getPdfBase64();
	}

}
