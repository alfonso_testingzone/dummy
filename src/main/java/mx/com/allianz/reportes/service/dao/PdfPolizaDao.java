package mx.com.allianz.reportes.service.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import mx.com.allianz.reportes.service.model.PdfPoliza;
import mx.com.allianz.reportes.service.model.PdfPolizaPk;

@Transactional
public interface PdfPolizaDao extends CrudRepository<PdfPoliza, PdfPolizaPk> {

}
