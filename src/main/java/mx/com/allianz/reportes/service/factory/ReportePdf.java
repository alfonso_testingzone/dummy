package mx.com.allianz.reportes.service.factory;

import java.awt.Color;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

import mx.com.allianz.commons.dto.daf.PolizaDTO;

public abstract class ReportePdf{
	
	public static final String PDF_ROOTPATH = //"/Users/sfrancof/JaspersoftWorkspace/pdf_poliza/";
			"/home/ec2-user/reportes/";
	protected static final String VALOR_NULO = " - ";
	protected static final Color AZUL_CLARITO = new Color(78,128,188);
	protected static final Color AZUL_FUERTE = new Color(6,63,123);
	protected static final Font smallNormal = new Font(Font.HELVETICA, 10, Font.NORMAL);
	protected static final Font smallBold = new Font(Font.HELVETICA, 10, Font.BOLD);
	protected static final Font titleFont = new Font(Font.HELVETICA, 12, Font.NORMAL, Color.WHITE);
	protected static final Font zapfdingbats = new Font(Font.ZAPFDINGBATS, 11, Font.NORMAL, new Color(96, 147, 195));
	protected static final Font footerFont = new Font(Font.HELVETICA, 8, Font.NORMAL, new Color(12, 119, 195));
	
	protected class HeaderFooter extends PdfPageEventHelper {

    	private String producto;
    	
    	public HeaderFooter(String producto){
    		this.producto = producto;
    	}
    	
    	@Override
    	public void onEndPage(PdfWriter writer, Document document) {
            PdfContentByte cb = writer.getDirectContent();
            Phrase footer = new Phrase(writer.getPageNumber() + " - Blvd. Manuel Ávila Camacho 164, Col. Lomas de Barrilaco, Ciudad de México. C.P. 11010 ", footerFont);
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT,
                    footer,
                    document.right(),
                    document.bottom() - 4, 0);
            footer = new Phrase("Tel.: 5201 3000, Lada sin Costo: 01 800 11 11 2 00 • www.allianz.com.mx", footerFont);
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT,
                    footer,
                    document.right(),
                    document.bottom() - 12, 0);
        }
    	
    	public void onStartPage(PdfWriter writer, Document document) {
			try {
				document.add(addHeader(producto));
			} catch (Exception e){
				e.printStackTrace();
			}
    	}
    }
	
	protected void addMetaData(Document document, PolizaDTO poliza) {
        document.addTitle("Poliza de ...");
        document.addSubject("Poliza de ...");
        document.addKeywords("Allianz, seguros, poliza");
        document.addAuthor("Allianz México, S.A.");
        document.addCreator("Allianz México, S.A.");
    }
	
	protected PdfPTable addHeader(String producto) {
    	PdfPCell c1;
    	Image img;
    	PdfPTable table = new PdfPTable(new float[] { 2, 4, 2 });
		try {
			table.setTotalWidth(200);
	        table.setWidthPercentage(100);

			img = Image.getInstance("src/main/resources/allianzlogo.png");
			img.scaleToFit(125,125);
			  
			c1 = crearCeldaNegritaSinBorde("");
			c1.addElement(new Paragraph("Allianz México, S.A.",smallBold));
			c1.addElement(new Paragraph("Compañia de seguros",smallNormal));
			c1.addElement(new Paragraph(" ",smallNormal));
			table.addCell(c1);
			table.addCell(crearCeldaSinBordeCentrada(producto));
			c1 = new PdfPCell(img);
			c1.setBorder(Rectangle.NO_BORDER);
			table.addCell(c1);
	    } catch (Exception x) {
	      x.printStackTrace();
	    }
		return table;
    }

	protected Paragraph crearOpcionCheck(String texto, boolean checked, Font fuente){
    	return crearOpcion(true, texto, checked, fuente);
    }
    
	protected Paragraph crearOpcionRadio(String texto, boolean checked, Font fuente){
    	return crearOpcion(false, texto, checked, fuente);
    }
    
	protected Paragraph crearOpcion(boolean esCheck, String texto, boolean checked, Font fuente){
    	Chunk chunk = new Chunk(checked ? (esCheck ? "n " : "m ") : ( esCheck ? "o " : "l "), zapfdingbats);
    	Paragraph paragraph = new Paragraph("",fuente);
    	paragraph.add(chunk);
    	paragraph.add(texto);
    	return paragraph;
    }
    
	protected Paragraph crearParagraph(String texto, String valor, Font fuente){
    	Paragraph paragraph = new Paragraph(texto,fuente);
    	if ( valor != null)
    		paragraph.add(new Chunk(valor));
    	return paragraph;
    }
    
	protected PdfPCell crearCeldaTitulo(String titulo, int colspan){
    	PdfPCell c1 = new PdfPCell(new Paragraph(titulo, titleFont));
    	c1.setBackgroundColor(AZUL_FUERTE);
    	if ( colspan > 1 )
    		c1.setColspan(colspan);
        c1.setBorder(Rectangle.NO_BORDER);
        return c1;
    }
    
    
	protected PdfPCell crearCeldaAzulita(String texto){
    	PdfPCell c1 = crearCelda(texto, null, smallNormal, AZUL_CLARITO, Element.ALIGN_CENTER);
    	c1.setVerticalAlignment(Element.ALIGN_CENTER);
    	return c1;
    }
    
	protected PdfPCell crearCeldaAzulita(Paragraph texto){
    	PdfPCell c1 = crearCelda(texto, null, AZUL_CLARITO, Element.ALIGN_CENTER);
    	c1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
    	return c1;
    }
    
	protected PdfPCell crearCeldaSinBorde(String texto){
        return crearCelda(texto, null, smallNormal, null ,Element.ALIGN_JUSTIFIED);
    }
    
	protected PdfPCell crearCeldaSinBordeCentrada(String texto){
        return crearCelda(texto, null, smallNormal, null ,Element.ALIGN_CENTER);
    }
    
	protected PdfPCell crearCeldaNegritaSinBorde(String texto){
        return crearCelda(texto, null, smallBold, null ,Element.ALIGN_JUSTIFIED);
    }
    
	protected PdfPCell crearCeldaSinBorde(String texto, String valor){
        return crearCelda(texto, valor, smallNormal, null , Element.ALIGN_JUSTIFIED);
    }
    
	protected PdfPCell crearCeldaColspan(String texto, String valor, int colspan){
        PdfPCell celda = crearCelda(texto, valor, smallNormal, null ,Element.ALIGN_JUSTIFIED);
        celda.setColspan(colspan);
    	return celda;
    }
    
	protected PdfPCell crearCelda(String texto, String valor,Font font, Color color, int alignment){
    	PdfPCell c1;
    	Paragraph phrase = new Paragraph(texto,font);
    	if ( valor != null)
    		phrase.add(new Chunk(valor));
    	c1 = new PdfPCell(phrase);
    	if ( alignment > -1 )
    		c1.setHorizontalAlignment(alignment);
        if (color != null)
        	c1.setBorderColor(color);
        else
        	c1.setBorder(0);
        
        return c1;
    }
    
	protected PdfPCell crearCelda(Paragraph texto, String valor, Color color, int alignment){
    	PdfPCell c1;
    	if ( valor != null)
    		texto.add(new Chunk(valor));
    	c1 = new PdfPCell(texto);
    	if ( alignment > -1 )
    		c1.setHorizontalAlignment(alignment);
        if (color != null)
        	c1.setBorderColor(color);
        else
        	c1.setBorder(0);
        
        return c1;
    }
    
    
	protected PdfPCell crearCelda(Paragraph texto){
    	PdfPCell c1;
    	c1 = new PdfPCell(texto);
    		
        c1.setBorder(0);
        return c1;
    }
	
}
