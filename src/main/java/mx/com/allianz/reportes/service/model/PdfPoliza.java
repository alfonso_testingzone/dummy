package mx.com.allianz.reportes.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "PDF_POLIZA")
@IdClass(PdfPolizaPk.class)
public class PdfPoliza {
	
	@Id
	@Column(name="ID_POLIZA")
	private String idPoliza;
	
	@Id
	@Column(name="TIPO_DOCUMENTO")
	private String tipoDocumento;
	
	@Column(name="PDF_BASE64",columnDefinition="CLOB NOT NULL")
    @Lob
	private String pdfBase64;
	
	
	public PdfPoliza(){
		super();
	}

	public PdfPoliza(String idPoliza, String tipoDocumento, String pdfBase64) {
		this();
		this.idPoliza = idPoliza;
		this.tipoDocumento = tipoDocumento;
		this.pdfBase64 = pdfBase64;
	}

	public String getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(String idPoliza) {
		this.idPoliza = idPoliza;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoCuestionario) {
		this.tipoDocumento = tipoCuestionario;
	}

	public String getPdfBase64() {
		return pdfBase64;
	}

	public void setPdfBase64(String pdfBase64) {
		this.pdfBase64 = pdfBase64;
	}

	@Override
	public String toString() {
		return "PdfCuestionario [idPoliza=" + idPoliza + ", tipoCuestionario=" + tipoDocumento + ", pdfBase64="
				+ pdfBase64 + "]";
	}
	
	
	
}
