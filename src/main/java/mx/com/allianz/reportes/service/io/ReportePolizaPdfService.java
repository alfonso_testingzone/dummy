package mx.com.allianz.reportes.service.io;

import mx.com.allianz.commons.dto.daf.PolizaDTO;

public interface ReportePolizaPdfService {

	public String crearPdf(PolizaDTO poliza);
	public boolean guardarPDF(String pdfBase64, String idPoliza);
	public String obtenerPolizaPDF(String idPoliza);
}
