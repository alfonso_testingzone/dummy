package mx.com.allianz.reportes.service.factory.impl;

import java.awt.Color;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chapter;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Section;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import mx.com.allianz.commons.dto.daf.AlternativaInversionDTO;
import mx.com.allianz.commons.dto.daf.AportacionDTO;
import mx.com.allianz.commons.dto.daf.BeneficiarioDTO;
import mx.com.allianz.commons.dto.daf.CatalogosDTO;
import mx.com.allianz.commons.dto.daf.CatalogosIucDTO;
import mx.com.allianz.commons.dto.daf.ClienteDTO;
import mx.com.allianz.commons.dto.daf.CuestionarioDTO;
import mx.com.allianz.commons.dto.daf.DocumentoDTO;
import mx.com.allianz.commons.dto.daf.IdentificadoresDTO;
import mx.com.allianz.commons.dto.daf.InversionDTO;
import mx.com.allianz.commons.dto.daf.InvestigacionDTO;
import mx.com.allianz.commons.dto.daf.LaboralDTO;
import mx.com.allianz.commons.dto.daf.PagoDTO;
import mx.com.allianz.commons.dto.daf.PolizaDTO;
import mx.com.allianz.commons.dto.daf.RespuestaDTO;
import mx.com.allianz.commons.dto.daf.TarjetaDTO;
import mx.com.allianz.commons.dto.daf.domicilio.DireccionDTO;
import mx.com.allianz.constants.TipoCodigoBancario;
import mx.com.allianz.constants.TipoDocumento;
import mx.com.allianz.reportes.service.dao.PdfPolizaDao;
import mx.com.allianz.reportes.service.factory.ReportePdf;
import mx.com.allianz.reportes.service.io.ReportePolizaPdfService;
import mx.com.allianz.reportes.service.model.PdfPoliza;
import mx.com.allianz.reportes.service.model.PdfPolizaPk;

@Service
public class ReportePolizaPDF extends ReportePdf implements ReportePolizaPdfService{
	
	@Autowired
	private PdfPolizaDao pdfPolizaDao;
	
	public static final String CLAVE_POLIZA = "POLIZA";
	private static final String CLAVE_COBERTURAS = "CUESTIONARIO_COBERTURAS";
	public static String FILE_POLIZA = "pdf_poliza.pdf";

	@Override
	public String crearPdf(PolizaDTO poliza) {
		HeaderFooter event = new HeaderFooter("\n" + (poliza.getAportacion() != null && poliza.getAportacion().getProductos() != null ? 
        		poliza.getAportacion().getProductos().getDescProd() : VALOR_NULO));
		try {
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, 
            		new FileOutputStream( PDF_ROOTPATH + ( FILE_POLIZA )));
            document.open();
            writer.setPageEvent(event);
            addMetaData(document, poliza);
            addContent(document, poliza);
            document.close();
        } catch (Exception e) {
        	System.out.println("Tronó al ejecutar la generación de la poliza:"+e.getMessage());
        	e.printStackTrace();
        }
        System.out.println("Reporte poliza creado exitosamente");
		return null;
	}

	@Override
	public boolean guardarPDF(String PdfBase64, String idPoliza) {
		PdfPoliza poliza = new PdfPoliza(idPoliza,CLAVE_POLIZA,PdfBase64);
		poliza = pdfPolizaDao.save(poliza);
		return poliza != null;
	}
	
	
	private void addContent(Document document, PolizaDTO poliza) throws DocumentException {
        Chapter catPart = new Chapter( 1 );
        catPart.setNumberDepth(0);

        Section subCatPart = catPart.addSection("");
        subCatPart.setNumberDepth(0);
        
        subCatPart.add(addHeader( "\n" + (poliza.getAportacion() != null && poliza.getAportacion().getProductos() != null ? 
        		poliza.getAportacion().getProductos().getDescProd() : VALOR_NULO)));
        try{
        agregaSeccion01DatosDelAsegurado( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 1");
		}
        try{
        agregaSeccion02InformacionLaboralFinanciera( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 2");
		}
        try{
        agregaSeccion03FondoEnAdministracion( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 3");
		}
        try{
        agregaSeccion04Coberturas( subCatPart,  poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 4");
		}
        try{
        agregaSeccion05DesignacionDeBeneficiarios( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 5");
		}
        try{
        agregaSeccion06DocumentosRegistrados( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 6");
		}
        try{
        agregaSeccion07y8MetodoDeAutenticacionyEstadosDeCuenta( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 7 u 8");
		}
        try{
        agregaSeccion09ConductoParaRealizarAportacion( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 9");
		}
        try{
        agregaSeccion10AutorizacionParaCobroDeAportaciones( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 10");
		}
        try{
        agregaSeccion11DeInteresParaElSolicitante( subCatPart, poliza );
        }catch (Exception e) {
        	System.out.println("Tronó en seccion 11");
		}

        // now add all this to the document
        document.add(catPart);

    }
	
	private void agregaSeccion01DatosDelAsegurado(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {
    	PdfPCell c1;
    	ClienteDTO cliente = poliza.getCliente() != null ? poliza.getCliente() : new ClienteDTO();
    	DireccionDTO direccion = poliza.getDomicilio() != null ? poliza.getDomicilio() : new DireccionDTO();
    	List<CatalogosIucDTO> identificacion = poliza.getIdentificacion() != null ? poliza.getIdentificacion() : new ArrayList<>();
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        table.addCell( crearCeldaTitulo("I. Datos del Asegurado",3));
        table.setHeaderRows(1);
//
        table.addCell(crearCeldaSinBorde("Nombre: ",cliente.getNombre() != null ? cliente.getNombre(): VALOR_NULO));
        
        table.addCell(crearCeldaSinBorde("Apellido Paterno: ", cliente.getApellidoPaterno() != null ?
        		cliente.getApellidoPaterno() : VALOR_NULO));
        table.addCell(crearCeldaSinBorde("Apellido Materno: ", cliente.getApellidoMaterno() != null ? 
        		cliente.getApellidoMaterno() : VALOR_NULO));
        table.addCell(crearCeldaSinBorde("Fecha de Nacimiento: ", 
        		cliente.getFechaNacimiento() != null ? formatter.format(cliente.getFechaNacimiento()) : VALOR_NULO ));
        table.addCell(crearCeldaSinBorde("Nacionalidad: ", 
        		cliente.getNacionalidad() != null ? cliente.getNacionalidad().getDescripcion() : VALOR_NULO));
        table.addCell(crearCeldaSinBorde("Sexo: ",cliente.getGenero() != null ? cliente.getGenero().getDescripcion() : VALOR_NULO));
        table.addCell(crearCeldaSinBorde("RFC: ", obtenIuc(identificacion,"RFC")));
        table.addCell(crearCeldaSinBorde("CURP: ", obtenIuc(identificacion,"CURP")));
        table.addCell(crearCeldaSinBorde("INE: ", obtenIuc(identificacion,"INE")));
        table.addCell(crearCeldaSinBorde("Calle: ",direccion.getCalle() != null ? direccion.getCalle() : ""));
        table.addCell(crearCeldaSinBorde("No. Exterior: ", direccion.getNumeroExterior() != null ? direccion.getNumeroExterior() : VALOR_NULO));
        table.addCell(crearCeldaSinBorde("No. Interior: ", direccion.getNumeroInterior() != null ? direccion.getNumeroInterior() : VALOR_NULO ));
        table.addCell(crearCeldaSinBorde("Colonia: ", direccion.getColonia() != null ? direccion.getColonia().getDescripcion() : VALOR_NULO ));
        table.addCell(crearCeldaSinBorde("Delegación o Municipio: ", direccion.getMunicipio() != null ? direccion.getMunicipio().getDescripcion() : VALOR_NULO ));
        table.addCell(crearCeldaSinBorde("Ciudad: ", direccion.getCiudad() != null ? direccion.getCiudad().getDescripcion() : VALOR_NULO ));
        table.addCell(crearCeldaSinBorde("C.P.: ", direccion.getCodigoPostal() > 0 ? direccion.getCodigoPostal() + "" : VALOR_NULO ));
        table.addCell(crearCeldaSinBorde("Estado: ", direccion.getEstado() != null ? direccion.getEstado().getDescripcion() : VALOR_NULO ));
        table.addCell(crearCeldaSinBorde("País de nacimiento: ", direccion.getPais() != null ? direccion.getPais().getDescripcion() :VALOR_NULO));
        table.addCell(crearCeldaSinBorde("Teléfono móvil: ", poliza.getContacto().getTelefono() != null ? poliza.getContacto().getTelefono() : VALOR_NULO));
        table.addCell(crearCeldaColspan("Correo Electrónico: ", poliza.getContacto().getCorreo() != null ? poliza.getContacto().getCorreo() : VALOR_NULO , 2));

        subCatPart.add(table);
        
        table = new PdfPTable(new float[] { 9, 1, 1 });
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.addCell(crearCeldaSinBorde("*Autorizo a Allianz México, S.A. Cía de Seguros a recibir instrucciones sobre mi Fondo Individual en Administración a través de este E-mail (Correo Electrónico Autorizado)."));
        c1 = crearCelda(crearOpcionCheck("Sí",true,smallNormal));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.addCell(crearOpcionCheck("No", false, smallNormal));
        
        subCatPart.add(table);
        subCatPart.add(new Paragraph(" "));
    }
    
    private void agregaSeccion02InformacionLaboralFinanciera(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {

    	PdfPCell c1;
    	InvestigacionDTO investigacion = poliza.getInvestigacion() != null ? poliza.getInvestigacion() : new InvestigacionDTO();
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    	LaboralDTO laboral = poliza.getLaboral() != null ? poliza.getLaboral() : new LaboralDTO();
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        
        table.addCell( crearCeldaTitulo("II. Información Laboral/Financiera",2));

        //poliza.getLaboral().g
        table.addCell(crearCeldaSinBorde("Giro o Actividad: ", laboral.getGiro() != null ? laboral.getGiro().getDescripcion() : VALOR_NULO));
        table.addCell(crearCeldaSinBorde("Ocupación: ", laboral.getOcupacion() != null ? laboral.getOcupacion().getDescripcion() : VALOR_NULO));
        table.addCell(crearCeldaSinBorde("Principal Fuentes de Ingresos: ", laboral.getIngresos() != null ? laboral.getIngresos().getDescripcion() : VALOR_NULO));
        table.addCell(crearCeldaSinBorde("No. Serie Certificado Digital (FEA): ", laboral.getNumeroSerie() + ""));// String.format("%.0f", laboral.getNumeroSerie()) ));
        
        subCatPart.add(table);
        
        table = new PdfPTable(new float[] { 9, 1, 1 });
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.addCell(crearCeldaSinBorde("¿Usted, su cónyuge o pariente de hasta segundo grado ha desempeñado funciones públicas destacadas, ha sido jefe de estado o de gobierno, líder político, funcionario gubernamental, judicial o militar de alta jerarquía, alto ejecutivo de empresas estatales o miembros de partidos políticos? ",null));
        c1 = crearCelda(crearOpcionCheck("Sí", poliza.getInvestigacion() != null,smallNormal));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.addCell(crearCelda(crearOpcionCheck("No", poliza.getInvestigacion() == null,smallNormal)));
        
        subCatPart.add(table);
        ////
        if ( poliza.getInvestigacion() != null ){
        	table = new PdfPTable(3);
        	table.setWidthPercentage(100);
        	table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        	table.addCell(crearParagraph("Nombre: ", investigacion.getNombre() != null && investigacion.getApellidoPaterno() != null?
        			new StringBuilder(investigacion.getNombre()).append(" ").append(investigacion.getApellidoPaterno() != null ? investigacion.getApellidoPaterno() : "")
        			.append(" ").append(investigacion.getApellidoMaterno() != null ? investigacion.getApellidoMaterno() : "" ).toString() :" - " , smallNormal) );
        	table.addCell(crearParagraph("Puesto: ", investigacion.getDescripcionPuesto() != null ? investigacion.getDescripcionPuesto() : VALOR_NULO, smallNormal));
        	table.addCell(crearParagraph("Parentesco: ", investigacion.getParentesco() != null ? investigacion.getParentesco().getDescripcion() : VALOR_NULO , smallNormal));
        	table.addCell(crearParagraph("Inicio de Funciones: ", investigacion.getInicioFunciones() != null ? formatter.format(investigacion.getInicioFunciones()) : VALOR_NULO, smallNormal));
        	table.addCell(crearCeldaColspan("Fin de Funciones: ", investigacion.getTerminoFunciones() != null ? formatter.format(investigacion.getTerminoFunciones()) : VALOR_NULO, 2));
        	
        	subCatPart.add(table);
        }
        
        
        subCatPart.add(new Paragraph(" "));
    }
    
    
    private void agregaSeccion03FondoEnAdministracion(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {
    	AportacionDTO aportacion = poliza.getAportacion() != null ? poliza.getAportacion() : new AportacionDTO();
    	InversionDTO inversion = poliza.getInversion() != null ? poliza.getInversion() : new InversionDTO();
    	List<AlternativaInversionDTO> alternativas = poliza.getAlternativas();
    	PdfPCell c1 ;
    	PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(100);
        table.addCell( crearCeldaTitulo("III. Fondo en Administración",4));
        
        table.addCell(crearCeldaSinBorde("Monto de la Aportación: ", String.format("%.2f", aportacion.getMontoAportacion() )));
        c1 = crearCelda(crearOpcionCheck("Pesos Revaluables", false, smallNormal));
        c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(c1);
        c1 = crearCelda(crearOpcionCheck("Moneda Nacional   ", true, smallNormal));
        c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(c1);
        table.addCell(crearCelda(crearOpcionCheck("Dólares",false,smallNormal)));
        table.addCell(crearCeldaColspan("Periodicidad de la Aportación: ", aportacion.getPeriodicidades() != null ? ( aportacion.getPeriodicidades().getDescripcion() != null ? aportacion.getPeriodicidades().getDescripcion() : aportacion.getPeriodicidades().getClave() ) : VALOR_NULO ,2));
        table.addCell(crearCeldaColspan("Plazo del Fondo en Administración: ",aportacion.getPlazo() != null ? aportacion.getPlazo().getDescripcion() : VALOR_NULO ,2));
        table.addCell(crearCeldaColspan("Monto Anual: ", String.format("%.2f", aportacion.getMontoAportacion() * 12 / (aportacion.getPeriodicidades() != null ? new Integer( aportacion.getPeriodicidades().getClave()) : 1)) ,2)); //TODO Checar de donde sacar los datos de estos dos campos
        table.addCell(crearCeldaColspan("Suma Total Comprometida: ",String.format("%.2f", (aportacion.getPlazo() != null ? new Integer(aportacion.getPlazo().getClave()) : 0)*aportacion.getMontoAportacion() * 12 / (aportacion.getPeriodicidades() != null ? new Integer( aportacion.getPeriodicidades().getClave()) : 1)) ,2));
        
        table.addCell(crearCeldaSinBorde("Tipo de Inversionista: "));
        table.addCell(crearCelda(crearOpcionCheck("Conservador  ",inversion != null && "Conservador".equals(inversion.getPerfilInversionistaSeleccionado()),smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Moderado  ",inversion != null && "Moderado".equals(inversion.getPerfilInversionistaSeleccionado()),smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Agresivo  ",inversion != null && "Agresivo".equals(inversion.getPerfilInversionistaSeleccionado()),smallNormal)));
        
        table.addCell(crearCeldaColspan("Asignación de la Aportación entre las diferentes Alternativas de Inversión: ",null,4));
        
        subCatPart.add(table);
        
        
        table = new PdfPTable(3);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorderWidth(4f);
        table.getDefaultCell().setBorderColor(AZUL_CLARITO);
        
        table.addCell(crearCeldaAzulita("Tipo - Divisa"));
        table.addCell(crearCeldaAzulita("Monto"));
        table.addCell(crearCeldaAzulita("Porcentaje"));
        table.setHeaderRows(1);
        
        if (alternativas != null && !alternativas.isEmpty())
        	for (AlternativaInversionDTO alternativa:alternativas){
        		table.addCell(crearCeldaAzulita(new StringBuilder( alternativa.getPerfilInversion() != null ? alternativa.getPerfilInversion().getDescripcion() : "" )
        				.append(VALOR_NULO).append(alternativa.getGrupoInversion() != null ? alternativa.getGrupoInversion().getDescripcion() : "").toString()));
                table.addCell(crearCeldaAzulita(alternativa.getMontoInversion() + ""));
                table.addCell(crearCeldaAzulita(alternativa.getPorcentajeInversion() + ""));
        	}
        else {
        	c1 = crearCeldaAzulita("No se registraron aportaciones");
        	c1.setColspan(3);
        	table.addCell(c1);
        }
        
        subCatPart.add(table);
        subCatPart.add(new Paragraph(" "));
        
        table = new PdfPTable(3);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorder(2);
        table.getDefaultCell().setBorderColor(new Color(78,128,188));
        
        table.addCell(crearCeldaColspan("Estimación de Aportaciones y Retiros Mensuales: ", String.format("%.2f",  aportacion.getMontoAportacion() / (aportacion.getPeriodicidades() != null ? new Integer( aportacion.getPeriodicidades().getClave()) : 1)) , 3));//TODO revisar de donde se obtiene, no está en objeto laboral
        
//        table.addCell(crearCeldaAzulita("Tipo de Transacción"));
//        table.addCell(crearCeldaAzulita("Número de Transacciones Estimadas por mes"));
//        table.addCell(crearCeldaAzulita("Monto Mensual en Total de Transacciones"));
//        table.setHeaderRows(1);
//        //Cuestionarios TODO revisar seteo de estos datos
//        table.addCell(crearCeldaAzulita("Depósitos"));
//        table.addCell(crearCeldaAzulita(" "));
//        table.addCell(crearCeldaAzulita(" "));
//        table.addCell(crearCeldaAzulita("Retiros"));
//        table.addCell(crearCeldaAzulita(" "));
//        table.addCell(crearCeldaAzulita(" "));
        
        subCatPart.add(table);
        
        subCatPart.add(new Paragraph(" "));
    }
    
    private void agregaSeccion04Coberturas(Section subCatPart, PolizaDTO poliza){
    	List<RespuestaDTO> coverturas = null;
    	List<CuestionarioDTO> cuestionarios = poliza.getCuestionario() != null ? poliza.getCuestionario(): new ArrayList<>();
    	PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.addCell( crearCeldaTitulo("IV. Coberturas",0));
        subCatPart.add(table);
        //subCatPart.add(crearParagraph("PLAN: ",null,smallNormal));
        
        for ( CuestionarioDTO cuestionario : cuestionarios )
       	 if( CLAVE_COBERTURAS.equals(cuestionario.getTipoCuestionario()) ){
       		 coverturas = cuestionario.getRespuesta();
       		 break;
       	 }
        
        
        
        if ( coverturas != null && !coverturas.isEmpty() ){
        	subCatPart.add(new Paragraph("COBERTURAS",smallNormal));
			for ( RespuestaDTO covertura : coverturas )
				subCatPart.add(crearOpcionCheck(covertura.getDescripcionPregunta(), 
						 Boolean.TRUE.toString().equals(covertura.getValorRespuesta()), smallNormal));
			 
		 } else 
			 subCatPart.add(new Paragraph("No se registraron coberturas",smallNormal));
		 
    }
    
    private void agregaSeccion05DesignacionDeBeneficiarios(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {
    	PdfPCell c1;
    	List<BeneficiarioDTO> beneficiarios = poliza.getBeneficiario() != null ? poliza.getBeneficiario() : new ArrayList<>();
    	subCatPart.add(new Paragraph(" "));
        
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        
        table.addCell( crearCeldaTitulo("V. Designación de Beneficiarios",0));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("La suma de los porcentajes debe ser igual al 100%."));
        subCatPart.add(table);
        
        
        table = new PdfPTable(new float[] { 2, 1, 1 });
        table.setWidthPercentage(100);
        table.addCell(crearCeldaAzulita("Nombre del Beneficiario "));
        table.addCell(crearCeldaAzulita("Parentesco"));
        table.addCell(crearCeldaAzulita("Porcentaje"));
        table.setHeaderRows(1);
        
        if ( beneficiarios != null && !beneficiarios.isEmpty() )
        	for(BeneficiarioDTO beneficiario : beneficiarios){
        		table.addCell(crearCeldaAzulita(beneficiario.getNombreBeneficiario()));
        		table.addCell(crearCeldaAzulita(beneficiario.getParentesco().getDescripcion()));
        		table.addCell(crearCeldaAzulita(beneficiario.getPorcentaje() + ""));
        	}
        else {
        	c1 = crearCeldaAzulita("No se registraron beneficiarios");
        	c1.setColspan(3);
        	table.addCell(c1);
        }
        
        subCatPart.add(table);
        
        subCatPart.add(new Paragraph(" "));
        table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.addCell(crearCeldaSinBorde("Advertencia:  En el caso de que se desee nombrar beneficiarios a menores de edad, no se debe señalar a un mayor de edad como representante de los menores para efectos de que, en su representación, cobre la indemnización. Lo anterior porque las legislaciones civiles previenen la forma en que deben designarse tutores, albaceas, representantes de herederos u otros cargos similares y no consideran al contrato de seguro como el instrumento adecuado para tales designaciones."));
        table.addCell(crearCeldaSinBorde("La designación que se hiciera de un mayor de edad como representante de menores beneficiarios durante la minoría de edad de ellos, legalmente puede implicar que se nombra beneficiario al mayor de edad, quien en todo caso sólo tendría la obligación moral, pues la designación que se hace de beneficiarios en un contrato de seguro le concede el derecho incondicionado de disponer de la suma asegurada."));
        subCatPart.add(table);
        subCatPart.add(new Paragraph(" "));
    }
    
    
    private void agregaSeccion06DocumentosRegistrados(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {
    	 PdfPTable table ;
    	 PdfPCell c1;
    	 List<DocumentoDTO> documentos = poliza.getDocumento() != null ? poliza.getDocumento() : new ArrayList<>();
         table = new PdfPTable(3);
         table.setWidthPercentage(100);
         table.addCell( crearCeldaTitulo("VI. Documentos Registrados",3));
         table.setHeaderRows(1);
         
         for (TipoDocumento documento: TipoDocumento.values()){
        	 table.addCell(crearCelda(crearOpcionCheck(documento.getNombre(),documentos.contains(new DocumentoDTO(
    				 new CatalogosDTO(documento.getValor(), null))),smallNormal)));
         }
         
         subCatPart.add(table);
         
         table = new PdfPTable(new float[] { 3, 1, 1 });
         table.setWidthPercentage(100);
         table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        //--------- poliza.getInversion().g
         table.addCell(crearCeldaSinBorde("Autorizo al Agente a tener acceso a la información de la póliza ",null));
         c1 = crearCelda(crearOpcionCheck("Sí", poliza.getInvestigacion() != null,smallNormal));
         c1.setHorizontalAlignment(Element.ALIGN_CENTER);
         table.addCell(c1);
         table.addCell(crearCelda(crearOpcionCheck("No", poliza.getInvestigacion() == null,smallNormal)));
         
         subCatPart.add(table);
         subCatPart.add(new Paragraph(" "));
    }
    
    private void agregaSeccion07y8MetodoDeAutenticacionyEstadosDeCuenta(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {
    	PdfPTable table = new PdfPTable(1);
    	List<IdentificadoresDTO> identificaciones = poliza.getBiometricos() != null ? 
    			poliza.getBiometricos().getIdentificadores() : new ArrayList<>();
        table.setWidthPercentage(100);
        table.addCell( crearCeldaTitulo("VII. Método de Autenticación y Confirmación de Solicitud de Póliza",0));
        table.addCell(crearCelda(crearOpcionCheck("Autenticación Biométrica vía Reconocimiento Facial.", 
        		identificaciones.contains(new IdentificadoresDTO("VOICEID",null,null)), smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Confirmación de Solicitud de Póliza con Muestra de Voz.", 
        		identificaciones.contains(new IdentificadoresDTO("FACEID",null,null)), smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Autenticación vía Correo Electrónico y Número Telefónico.", false, smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Confirmación de Solicitud de Póliza con Firma Electrónica.", false, smallNormal)));
        
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell( crearCeldaTitulo("VIII. Estados de Cuenta",0));
        table.addCell(crearCeldaSinBorde("Los estados de cuenta serán entregados a través del Portal de Clientes de Allianz"));
        
        subCatPart.add(table);
    }
    
    private void agregaSeccion09ConductoParaRealizarAportacion(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {
    	PdfPTable table = new PdfPTable(4);
    	TarjetaDTO tarjeta  = poliza.getTarjeta() != null  && !poliza.getTarjeta().isEmpty() ? poliza.getTarjeta().get(0) : null;
    	PagoDTO pago = poliza.getPago() != null ? poliza.getPago() : new PagoDTO();
        table.setWidthPercentage(100);
        table.addCell( crearCeldaTitulo("IX. Conducto para Realizar Aportación",4));
        table.addCell(crearCelda(crearOpcionCheck("Tarjeta de Crédito", tarjeta != null, smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Cuenta CLABE ", pago != null && pago.getClabe() != null && !pago.getClabe().isEmpty(), smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Efectivo", false, smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Cheque", false, smallNormal)));
        
        if ( tarjeta != null ){
	        table.addCell(crearCeldaColspan("Nombre de Tarjetahabiente: ", tarjeta.getNombreTarjeta(), 4));
	        table.addCell(crearCeldaColspan("Número de Tarjeta: **** **** **** ", tarjeta.getNumeroTarjeta() != null ? tarjeta.getNumeroTarjeta().substring(tarjeta.getNumeroTarjeta().length()-4) : VALOR_NULO, 2));
	        table.addCell(crearCeldaColspan("Fecha de Vencimiento: ", tarjeta.getFechaVencimiento() != null ? tarjeta.getFechaVencimiento() : VALOR_NULO, 2));
        }
        if ( pago != null && pago.getClabe() != null && !pago.getClabe().isEmpty()){
	        table.addCell(crearCeldaColspan("Cuenta CLABE:  ", pago.getClabe(), 2));  
	        table.addCell(crearCeldaColspan("Institución Bancaria: ", TipoCodigoBancario.valueOfValor(pago.getClabe().substring(0, 3)).getNombre(), 2));
        }
        
        table.addCell(crearCeldaColspan("En caso de que el depósito sea con cheque tendrá que ser a nombre de Allianz México, S.A. Compañía de Seguros y quedará aplicado salvo buen cobro. Al reverso deberá anotarse la referencia bancaria. Cualquier transferencia o depósito deberá de efectuarse a las cuentas de Allianz:  Cta. Banamex 0870-0587933 (Clabe 002 180 08700587933 9), Cta. Bancomer CIE-776750 y Cta. Scotiabank 001-00911216 (Clabe 044 180 00100911216 2), siendo indispensable anotar en la referencia el número de folio indicado en esta solicitud.", null, 4));
        
        subCatPart.add(new Paragraph(" "));
        subCatPart.add(table);
        
    }
    
    
    private void agregaSeccion10AutorizacionParaCobroDeAportaciones(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {
    	PagoDTO pago = poliza.getPago() ;
    	TarjetaDTO tarjeta  = poliza.getTarjeta() != null && !poliza.getTarjeta().isEmpty() ? poliza.getTarjeta().get(0) : null;
    	PdfPTable table = new PdfPTable(1);
    	subCatPart.add(new Paragraph(" "));
        table.setWidthPercentage(100);
        table.addCell( crearCeldaTitulo("X. Autorización para Cobro de aportaciones",0));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("Solicito que el medio de cobro para mi póliza sea: "));
        table.addCell(crearCelda(crearOpcionCheck("Vía depósito o transferencia bancaria (únicamente pólizas con forma de pago anual o semestral). ", poliza.getPagoSubsecuente() != null, smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Cargo automático a Tarjeta de Crédito.", tarjeta != null, smallNormal)));
        table.addCell(crearCelda(crearOpcionCheck("Cargo automático a Cuenta de Cheques por medio de Cuenta CLABE.", pago != null, smallNormal)));
        
        subCatPart.add(table);
        
        table = new PdfPTable(2);
        table.setWidthPercentage(100);
        if ( tarjeta != null ){
        	table.addCell(crearCeldaColspan("Nombre de Tarjetahabiente: ", tarjeta.getNombreTarjeta(), 2));
        	table.addCell(crearCeldaSinBorde("Número de Tarjeta: **** **** **** ", tarjeta.getNumeroTarjeta() != null ? tarjeta.getNumeroTarjeta().substring(tarjeta.getNumeroTarjeta().length()-4) : VALOR_NULO));
        	table.addCell(crearCeldaSinBorde("Fecha de Vencimiento: ", tarjeta.getFechaVencimiento() != null ? tarjeta.getFechaVencimiento() : VALOR_NULO));
        }
        if ( pago != null && pago.getClabe() != null && !pago.getClabe().isEmpty() ){
        	table.addCell(crearCeldaSinBorde("Cuenta CLABE:  ", pago.getClabe()));   
        	table.addCell(crearCeldaSinBorde("Institución Bancaria: ", TipoCodigoBancario.valueOfValor(pago.getClabe().substring(0, 3)).getNombre()));//Calcular mediante enum de instituciones
        }
        
        subCatPart.add(table);
        
        table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("Solicito y autorizo a Banco Nacional de México, SA integrante del Grupo Financiero Banamex o a aquella institución afiliada a VISA o a MasterCard para que, con base al contrato de apertura de crédito en cuenta corriente o el contrato de depósito en cuenta corriente según corresponda, que tengo celebrado y respecto del cual se me expidió la Tarjeta citada o en su caso el número de Tarjeta que por reposición de Ia anterior por robo o extravío de Ia misma me haya asignado el Banco, se sirvan pagar a mi cuenta a nombre de Allianz México S.A., Compañía de Seguros los cargos por los conceptos, periodicidad y montos que se detallan a continuación."));
        table.addCell(crearCeldaSinBorde("El negocio afiliado señalado en el rubro, se obliga y es responsable de cumplir con:  (i) La información generada correcta y oportuna de los cargos al Tarjetahabiente / Cuentahabiente, (ii). De Ia calidad de los productos y servicios ofrecidos, liberando a Banco Nacional de México, S.A. integrante del Grupo Financiero Banamex o a cualquier institución afiliada a VISA o a MasterCard de toda reclamación que se genere por parte del Tarjetahabiente / Cuentahabiente."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("El Tarjetahabiente / Cuentahabiente podrá revocar Ia Carta Autorización mediante comunicado por escrito con treinta días naturales de anticipación que recibirá Allianz México, S.A., Compañía de Seguros el cual anotará Ia fecha de su recepción con Ia firma y nombre de quien recibe por Allianz México S.A. Compañía de Seguros. En este caso Allianz México S.A., Compañía de Seguros deberá informar al Tarjetahabiente / Cuentahabiente Ia fecha en que dejará de surtir efecto Ia presente carta de autorización."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("Esta Carta Autorización estará vigente hasta nuevo aviso, mismo que notificaré por escrito con treinta días naturales de anticipación."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("El concepto, Ia periodicidad y el monto a cargar al Tarjetahabiente / Cuentahabiente, estarán sujetos a los acuerdos establecidos en Ia póliza de seguros contratada."));
        table.addCell(crearCeldaSinBorde(" "));
        
        subCatPart.add(table);
    }
    
    private void agregaSeccion11DeInteresParaElSolicitante(Section subCatPart, PolizaDTO poliza)
            throws BadElementException {
    	PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.addCell( crearCeldaTitulo("XI. De interés para el solicitante",0));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("Autorizo a Allianz México S.A. Compañía de Seguros, a tratar y, en su caso, transferir mis datos personales, sensibles y los patrimoniales o financieros, para todos los fines vinculados con la relación jurídica que tengamos celebrada, o en su caso, se celebre, así como para los indicados en el Aviso de Privacidad, disponible en la página de internet: ww.allianz.com.mx."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("En caso de haber proporcionado datos personales, sensibles, patrimoniales o financieros de otros titulares, me obligo a hacer del conocimiento de dichos titulares que he proporcionado tales datos a Allianz México S.A. Compañía de Seguros y los lugares en donde se encuentra a su disposición el referido Aviso de Privacidad."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("De acuerdo a la Ley Sobre el Contrato de Seguro, el solicitante debe declarar todos los hechos importantes para la apreciación del riesgo a que se refiere esta solicitud, tal como los conozca o deba conocer en el momento de firmar la misma, en la inteligencia de que la inexacta o falsa declaración de los hechos importantes que se pregunten para la apreciación del riesgo podría originar la pérdida de los derechos del Asegurado o de los Beneficiarios en su caso."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("Acepto recibir de forma electrónica la documentación contractual en formato PDF (portable document format), o cualquier otro formato electrónico equivalente, a través del correo electrónico que al efecto provea el solicitante, contratante o asegurado en la presente solicitud/cotización. Acepto que me fue informado que en caso de requerir un ejemplar impreso podré solicitarlo en cualquier momento directamente en las oficinas de Allianz México, S.A. Compañía de Seguros."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("Declaro que conozco el detalle de coberturas, limitaciones y exclusiones del producto solicitado en la presente, las cuales constan en las Condiciones Generales, mismas que están disponibles para su descarga en la sección Descarga de documentos en www.allianz.com.mx/descarga-de-documentos"));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("De conformidad con el artículo 96, fracción I, de la Ley de Instituciones de Seguros y de Fianzas, el Agente está obligado a informar de manera amplia y detallada el alcance real de la cobertura del seguro, así como la forma de conservarla o darla por terminada. Asimismo, le sugerimos detallar las características y los hechos importantes para la apreciación del riesgo."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("En términos del artículo 492, fracción I, de la Ley de Instituciones de Seguros y de Fianzas, le informamos que el Agente de Seguros y la Compañía Aseguradora tienen implementadas medidas y procedimientos para detectar actos, omisiones u operaciones que pudieran favorecer, prestar ayuda, auxilio o cooperación de cualquier especie para la comisión de los delitos previstos en los artículos 139 o 148 Bis del Código Penal Federal, o que pudieran ubicarse en los supuestos del artículo 400 Bis del mismo Código, por tal motivo, en caso de llegarse a verificar algún acto de los señalados anteriormente, originará la improcedencia en el pago, invalidando el seguro de forma automática."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("“Este documento sólo constituye una solicitud de seguro y, por tanto, no representa garantía alguna de la misma será aceptada por Allianz México, S.A. Compañía de Seguros, ni de que, en caso de aceptarse, concuerde totalmente con los términos de la solicitud”."));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("Para cualquier aclaración, queja, o duda no resuelta en relación con su seguro, contacte a la Unidad Especializada de Atención a Usuarios (UNE), en Blvd. Manuel Ávila Camacho 164, piso 1, Col. Lomas de Barrilaco, Ciudad de México C.P. 11010, en un horario de atención de lunes a jueves de 09:00 a 13:00 y de 15:00 a 17:00 horas y viernes de 09:00 a 14:00 horas, llamar a los teléfonos (55) 5201-3039 y del interior de la república al (01 800) 111 12 00 ext. 3039 o enviar un correo electrónico a claudia.espinosa@allianz.com.mx"));
        table.addCell(crearCeldaSinBorde(" "));
        table.addCell(crearCeldaSinBorde("Datos de la CONDUSEF: Av. Insurgentes Sur #762 Col. Del Valle, Ciudad de México. C.P. 03100. Teléfonos: (55) 5340 0999 y (01 800) 999 80 80, www.condusef.gob.mx, asesoría@condusef.gob.mx. En cumplimiento a lo dispuesto en el artículo 202 de la Ley de Instituciones de Seguros y de Fianzas, las documentaciones contractuales y las notas técnicas que integran estos productos de seguro, quedaron registradas ante la Comisión Nacional de Seguros y Fianzas, a partir del día 31 de enero de 2014, con el número CNSF-S0003-0075-2014; 7 de marzo de 2014, con el número CNSF-S0003-0145-2014; 5 de marzo de 2013, con el número CNSF-S0003-0106-2013."));
        
        
        subCatPart.add(table);
        
        
    }
    
    private String obtenIuc(List<CatalogosIucDTO> iucs,String iuc){
    	CatalogosIucDTO iuC = new CatalogosIucDTO(iuc,null);
    	if (iucs.contains(iuC))
    		return iucs.get(iucs.indexOf(iuC)).getValor();
    	return null;
    }// TODO Actualizar el jar en el server

	@Override
	public String obtenerPolizaPDF(String idPoliza) {
		PdfPoliza poliza = pdfPolizaDao.findOne(new PdfPolizaPk(idPoliza, "POLIZA"));
		return poliza.getPdfBase64();
	}
	

}
