package mx.com.allianz.reportes.service.factory.impl;

import java.io.FileOutputStream;

import com.lowagie.text.Chapter;
import com.lowagie.text.Document;
import com.lowagie.text.Section;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import mx.com.allianz.commons.dto.daf.PolizaDTO;
import mx.com.allianz.reportes.service.factory.ReportePdf;

public class DocumentoAdicionalPdf extends ReportePdf {
	
	private static String FILE_POLIZA = "pdf_poliza.pdf";

	//@Override
	public String crearPdf(PolizaDTO poliza) {
		HeaderFooter event = new HeaderFooter("\n" + (poliza.getAportacion() != null && poliza.getAportacion().getProductos() != null ? 
        		poliza.getAportacion().getProductos().getDescProd() : VALOR_NULO));
		try {
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, 
            		new FileOutputStream( PDF_ROOTPATH + ( FILE_POLIZA )));
            document.open();
            writer.setPageEvent(event);
            addMetaData(document, poliza);
            Chapter catPart = new Chapter( 1 );
            catPart.setNumberDepth(0);

            Section subCatPart = catPart.addSection("");
            subCatPart.setNumberDepth(0);
            
            subCatPart.add(addHeader( "\n" + (poliza.getAportacion() != null && poliza.getAportacion().getProductos() != null ? 
            		poliza.getAportacion().getProductos().getDescProd() : VALOR_NULO)));
            agregaDatos(subCatPart, poliza);
            document.close();
        } catch (Exception e) {
        	System.out.println("Tronó:"+e.getMessage());
        	e.printStackTrace();
        }
        System.out.println("Reporte poliza creado exitosamente");
		return null;
	}
	
	private void agregaDatos(Section subCatPart, PolizaDTO poliza){
		PdfPCell c1 = crearCeldaSinBorde(new StringBuilder("Le agradecemos que haya solicitado un plan ").append(poliza.getAportacion() != null && poliza.getAportacion().getProductos() != null ? poliza.getAportacion().getProductos().getDescProd() : VALOR_NULO)
				.append(". por su seguridad y buscando en todo momento su satisfacción como cliente, queremos confirmar que usted haya recibido la asesoria adecuada con base en sus necesidades y que haya entendido los beneficios y riesgos asociados al producto. ")
				.append("Por favor lea y firme, en caso de estar de acuerdo el presente Formato de Confirmación. Si el contenido de este documento no concuerda con su entendimiento del producto o con la asesoria proporcionada, no lo firme y comuníquese ")
				.append("con el area de Servicio al Cliente al 01800 1111 200 o bien al correo electrónico cliente.optimaxx@allianz.com.mx, en donde con gusto le atenderemos.").toString() ) ;
		PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(100);
        //table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        
        c1.setBorderWidthBottom(8f);
        c1.setBorderWidthTop(8f);
        c1.setBorderWidthRight(0f);
        c1.setBorderWidthLeft(0f);
        
        table.addCell(c1);
        
        subCatPart.add(table);
		subCatPart.add(crearParagraph("", null, smallNormal));
	}

	//@Override
	public boolean guardarPDF(String PdfBase64, String idPoliza) {
		throw new UnsupportedOperationException();
	}

	//@Override
	public String obtenerPolizaPDF(String idPoliza) {
		return null;
	}

}
