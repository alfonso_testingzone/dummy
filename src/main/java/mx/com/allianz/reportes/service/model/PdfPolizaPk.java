package mx.com.allianz.reportes.service.model;

import java.io.Serializable;

public class PdfPolizaPk implements Serializable {
	
	private String idPoliza;
	private String tipoDocumento;
	
	
	public PdfPolizaPk() { 
		super();
	}
	
	public PdfPolizaPk(String idPoliza, String tipoCuestionario) {
		this();
		this.idPoliza = idPoliza;
		this.tipoDocumento = tipoCuestionario;
	}
	public String getIdPoliza() {
		return idPoliza;
	}
	public void setIdPoliza(String idPoliza) {
		this.idPoliza = idPoliza;
	}
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public String toString() {
		return "PdfCuestionarioPk [idPoliza=" + idPoliza + ", tipoDocumento=" + tipoDocumento + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPoliza == null) ? 0 : idPoliza.hashCode());
		result = prime * result + ((tipoDocumento == null) ? 0 : tipoDocumento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PdfPolizaPk other = (PdfPolizaPk) obj;
		if (idPoliza == null) {
			if (other.idPoliza != null)
				return false;
		} else if (!idPoliza.equals(other.idPoliza))
			return false;
		if (tipoDocumento == null) {
			if (other.tipoDocumento != null)
				return false;
		} else if (!tipoDocumento.equals(other.tipoDocumento))
			return false;
		return true;
	}

	
	
}
