[
        {
            "uuidSolicitud": "bd0d1ac4-9f3a-4343-cebf-1qweqweqwe32342",
            "tipoCuestionario": "PERFIL_TRANSACCIONAL",
            "tituloCuestionario": "Perfil Transaccional",
            "versionCuestionario": "1.0.0",
            "respuesta": [
                {
                    "descripcionPregunta": "Transacciones estimadas por mes",
                    "codigoPregunta": "TRANS-MES-001",
                    "tipoPregunta": "4",
                    "valorRespuesta": "00003",
                    "descripcionRespuesta": "0 a 3 Transacciones por Mes",
                    "llaveOpcion": "TRANS-MES-001-001"
                },
                {
                    "descripcionPregunta": "Monto Mensual en Total de Transacciones",
                    "codigoPregunta": "TRANS-MES-002",
                    "tipoPregunta": "4",
                    "valorRespuesta": "00500000",
                    "descripcionRespuesta": "De 0 a 500 Mil Pesos",
                    "llaveOpcion": "TRANS-MES-002-001"
                },
                {
                    "descripcionPregunta": "Transacciones estimadas por mes",
                    "codigoPregunta": "TRANS-MES-003",
                    "tipoPregunta": "4",
                    "valorRespuesta": "00003",
                    "descripcionRespuesta": "0 a 3 Transacciones por Mes",
                    "llaveOpcion": "TRANS-MES-003-001"
                },
                {
                    "descripcionPregunta": "Operaciones estimadas por mes",
                    "codigoPregunta": "TRANS-MES-004",
                    "tipoPregunta": "4",
                    "valorRespuesta": "00500000",
                    "descripcionRespuesta": "De 0 a 500 Mil Pesos",
                    "llaveOpcion": "TRANS-MES-004-001"
                }
            ]
        },
        {
            "uuidSolicitud": "bd0d1ac4-9f3a-4343-cebf-1qweqweqwe32342",
            "tipoCuestionario": "PERFILAMIENTO_INVERSION",
            "tituloCuestionario": "Perfilamiento de Inversión",
            "versionCuestionario": "1.0.0",
            "respuesta": [
                {
                    "descripcionPregunta": "Nivel de estudios",
                    "codigoPregunta": "PERFI-INV-001",
                    "tipoPregunta": "4",
                    "valorRespuesta": "Pre",
                    "descripcionRespuesta": "Preparatoria",
                    "llaveOpcion": "PERFI-INV-001-003"
                },
                {
                    "descripcionPregunta": "Rango de edad",
                    "codigoPregunta": "PERFI-INV-002",
                    "tipoPregunta": "4",
                    "valorRespuesta": "3",
                    "descripcionRespuesta": "Hasta 40 años",
                    "llaveOpcion": "PERFI-INV-002-001"
                },
                {
                    "descripcionPregunta": "¿Cuáles de las siguientes opciones describe mejor su horizonte de inversión?",
                    "codigoPregunta": "PERFI-INV-003",
                    "tipoPregunta": "4",
                    "valorRespuesta": "3",
                    "descripcionRespuesta": "Más de 5 años",
                    "llaveOpcion": "PERFI-INV-003-003"
                },
                {
                    "descripcionPregunta": "Sociedades de inversión",
                    "codigoPregunta": "PERFI-INV-004",
                    "tipoPregunta": "1",
                    "valorRespuesta": "0",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "PERFI-INV-004-002"
                },
                {
                    "descripcionPregunta": "Instrumentos de deuda",
                    "codigoPregunta": "PERFI-INV-005",
                    "tipoPregunta": "1",
                    "valorRespuesta": "0",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "PERFI-INV-005-002"
                },
                {
                    "descripcionPregunta": "Acciones",
                    "codigoPregunta": "PERFI-INV-006",
                    "tipoPregunta": "1",
                    "valorRespuesta": "0",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "PERFI-INV-006-002"
                },
                {
                    "descripcionPregunta": "Derivados",
                    "codigoPregunta": "PERFI-INV-007",
                    "tipoPregunta": "1",
                    "valorRespuesta": "0",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "PERFI-INV-007-002"
                },
                {
                    "descripcionPregunta": "ETF`s'",
                    "codigoPregunta": "PERFI-INV-008",
                    "tipoPregunta": "1",
                    "valorRespuesta": "0",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "PERFI-INV-008-002"
                },
                {
                    "descripcionPregunta": "Inversiones con diferentes monedas",
                    "codigoPregunta": "PERFI-INV-009",
                    "tipoPregunta": "1",
                    "valorRespuesta": "0",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "PERFI-INV-009-002"
                },
                {
                    "descripcionPregunta": "Inversiones en el último año mayores o iguales a $10,000,000.",
                    "codigoPregunta": "PERFI-INV-010",
                    "tipoPregunta": "1",
                    "valorRespuesta": "0",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "PERFI-INV-010-002"
                },
                {
                    "descripcionPregunta": "Ingresos en los últimos dos años mayores o iguales a $2,500,000.",
                    "codigoPregunta": "PERFI-INV-011",
                    "tipoPregunta": "1",
                    "valorRespuesta": "0",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "PERFI-INV-011-002"
                }
            ]
        },
        {
            "uuidSolicitud": "bd0d1ac4-9f3a-4343-cebf-1qweqweqwe32342",
            "tipoCuestionario": "CUESTIONARIO_COBERTURAS",
            "tituloCuestionario": "Cuestionario Coberturas",
            "versionCuestionario": "1.0.0",
            "respuesta": [
                {
                    "descripcionPregunta": "Cobertura basica por muerte accidental",
                    "codigoPregunta": "CUEST-COB-001",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-COB-001-002"
                },
                {
                    "descripcionPregunta": "Seguro de blindaje",
                    "codigoPregunta": "CUEST-COB-002",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-COB-002-002"
                }
            ]
        },
        {
            "uuidSolicitud": "bd0d1ac4-9f3a-4343-cebf-1qweqweqwe32342",
            "tipoCuestionario": "CUESTIONARIO_MEDICO",
            "tituloCuestionario": "Cuestionario Medico",
            "versionCuestionario": "1.0.0",
            "respuesta": [
                {
                    "descripcionPregunta": "¿Padece o ha padecido de insuficiencia cardiaca, infartos, dolor en pecho o tórax, trastornos de la circulación o presión arterial alta?",
                    "codigoPregunta": "CUEST-MED-001",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-001-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-001-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-001-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-001-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Ha tenido alguna enfermedad o accidente que siga requiriendo de chequeos periódicos o tratamientos?",
                    "codigoPregunta": "CUEST-MED-002",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-002-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-002-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-002-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-002-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Ha tenido alguna enefermedad o accidente que siga requiriendo de chequeos periódicos o tratamientos?",
                    "codigoPregunta": "CUEST-MED-003",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-003-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-003-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-003-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-003-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Padece o ha padecido de bronquitis crónica, asma, tuberculosis o enfisema pulmonar?",
                    "codigoPregunta": "CUEST-MED-004",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-004-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-004-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-004-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-004-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Ha padecido de cualquier tipo de cáncer?",
                    "codigoPregunta": "CUEST-MED-005",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-005-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-005-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-005-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-005-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Padece o ha padecido de hepatitis, cirrosis, pancreatitis o cualquier otra enfermedad del higado o del páncreas?",
                    "codigoPregunta": "CUEST-MED-006",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-006-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-006-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-006-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-006-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Ha tenido resultados positivos en éxamenes de VIH SIDA?",
                    "codigoPregunta": "CUEST-MED-007",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-007-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-007-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-007-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-007-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Padece o ha padecido de diabetes o hipoglucemia(azúcar baja)?",
                    "codigoPregunta": "CUEST-MED-008",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-008-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-008-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-008-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-008-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Ha tenido resultados anormales en pruebas de laboratorio o diagnósticos (ultrasonido, resonancia, tomografía, rayos X o electrocardiogramas)?",
                    "codigoPregunta": "CUEST-MED-009",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-009-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-009-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-009-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-009-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Ha tenido algún tipo de quístes o tumoraciones?",
                    "codigoPregunta": "CUEST-MED-010",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-010-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-010-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-010-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-010-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Presenta actualmente síntomas o alteraciones de salud no atendidos médicamente?",
                    "codigoPregunta": "CUEST-MED-011",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-011-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-011-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-011-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-011-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Padece o ha padecido de cualquier otra enfermedad o lesión diferente a las preguntas en este cuestionario?",
                    "codigoPregunta": "CUEST-MED-012",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-012-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-012-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-012-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-012-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Ha estado hospitalizado?",
                    "codigoPregunta": "CUEST-MED-013",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-013-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-013-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-013-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-013-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Presenta actualmente síntomas o alteraciones de salud no atendidos médicamente?",
                    "codigoPregunta": "CUEST-MED-013",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-013-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-013-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-013-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-013-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Practica algún deporte peligroso? / ¿Cuál?",
                    "codigoPregunta": "CUEST-MED-014",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-MED-014-002"
                },
                {
                    "descripcionPregunta": "Enfermedad o lesion",
                    "codigoPregunta": "CUEST-MED-014-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha",
                    "codigoPregunta": "CUEST-MED-014-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "2017-09-19",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Estado Actual",
                    "codigoPregunta": "CUEST-MED-014-001-003",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                }
            ]
        },
        {
            "uuidSolicitud": "bd0d1ac4-9f3a-4343-cebf-1qweqweqwe32342",
            "tipoCuestionario": "CUESTIONARIO_HABITOS",
            "tituloCuestionario": "Cuestionario Habitos",
            "versionCuestionario": "1.0.0",
            "respuesta": [
                {
                    "descripcionPregunta": "¿Fuma?",
                    "codigoPregunta": "CUEST-HAB-001",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-HAB-001-002"
                },
                {
                    "descripcionPregunta": "¿Usted fuma cada?",
                    "codigoPregunta": "CUEST-HAB-001-001-001",
                    "tipoPregunta": "4",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Cuántos cigarros fuma por ocasión?",
                    "codigoPregunta": "CUEST-HAB-001-001-002",
                    "tipoPregunta": "4",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Consume alcohol?",
                    "codigoPregunta": "CUEST-HAB-002",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-HAB-002-002"
                },
                {
                    "descripcionPregunta": "¿Usted ingiere bebidas alcoholicas cada?",
                    "codigoPregunta": "CUEST-HAB-002-001-001",
                    "tipoPregunta": "4",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Cuántas bebidas consume por ocación?",
                    "codigoPregunta": "CUEST-HAB-002-001-002",
                    "tipoPregunta": "4",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Tipos de bebida",
                    "codigoPregunta": "CUEST-HAB-002-001-003",
                    "tipoPregunta": "4",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "¿Consume o ha consumido algún otro tipo de droga o estimulante?",
                    "codigoPregunta": "CUEST-HAB-003",
                    "tipoPregunta": "1",
                    "valorRespuesta": "false",
                    "descripcionRespuesta": "No",
                    "llaveOpcion": "CUEST-HAB-003-002"
                },
                {
                    "descripcionPregunta": "Clase de sustancia",
                    "codigoPregunta": "CUEST-HAB-003-001-001",
                    "tipoPregunta": "2",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "",
                    "llaveOpcion": ""
                },
                {
                    "descripcionPregunta": "Fecha de último consumo",
                    "codigoPregunta": "CUEST-HAB-003-001-002",
                    "tipoPregunta": "3",
                    "valorRespuesta": "",
                    "descripcionRespuesta": "Jan 01 2017",
                    "llaveOpcion": ""
                }
            ]
        }
    ]